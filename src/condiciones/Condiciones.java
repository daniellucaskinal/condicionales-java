/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package condiciones;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author a_lej
 */
public class Condiciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {        
        /*// pseudocode:
        //  jf n = 2 or n = 3 THEN
        //  jf n >= 100 and n <= 1000 THEN
        //  if n = 100
        //  if plabra = "Hola"
        String palabra = "Hola";
        String otraPalabra = new String("Hola");
        if (palabra.equals(otraPalabra)) {
            System.out.println("Saludo!");
        } else {
            System.out.println("ESte no es un saludo :(");
        } */
        
        /*
        // validacion de numeros pares
        int num = 10000;
        if (num % 2 == 0) {
            System.out.println("El numero ingresado es par!!!!!!!");
        } else {
            System.out.println("El numero NO es par :(");
        } */
        
        /* Calidacion de triangulo rectangulo
        int l1 = 4;
        int l2 = 3;
        int l3 = 5;
        // int hip2 = hip * hip;
        // int x2 = x * x;
        // int y2 = y * y;
        // int sum = x2 + y2;
        if (
                ((l1 * l1) == ((l2 * l2) + (l3 * l3))) ||
                ((l2 * l2) == ((l1 * l1) + (l3 * l3))) ||
                ((l3 * l3) == ((l1 * l1) + (l2 * l2)))
            ) {
            System.out.println("Es triangulo rectanfulo!!");
        } else {
            System.out.println("No es un triangulo rectangulo");
        } */
        
        // Manzanas
        int p1 = 5;
        int p2 = 5;
        int p3 = 4;
        
        int total = p1 + p2 + p3;
        int cu = total / 3;
        
        int sobra = total % 3;
        System.out.println("El total de manzanas son: " + total);
        System.out.println("Cada uno se llevará: " + cu);
        System.out.println("Sobran para mañana: " + sobra);
    }
    
}

